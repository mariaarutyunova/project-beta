import Lottie from "lottie-react";
import cars from './cars.json';


function ManufacturersList(props) {
    return (
      <div>
      <h1 id="manufacturers-title">Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {props.manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.href}>
                <td>{ manufacturer.name }</td>
                <td>{ manufacturer.id }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Lottie
          animationData={cars}
          className="lottie-center-bottom"
          style={{
            height: "30%",
            width: "33.33%",
            zIndex: -2,
          }}
          loop={true}
        />
      </div>

    );
  }

  export default ManufacturersList;
