function MainPage() {
  return (
    <div className="main-bg">
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Rev Realm <i className="fa-solid fa-car-side"></i></h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The site that helps you with all of your automotive dealership needs!
        </p>
      </div>
      </div>
    </div>
  );
}

export default MainPage;
