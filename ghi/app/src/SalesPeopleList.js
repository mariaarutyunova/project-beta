import React, { useState, useEffect } from 'react';
import Lottie from "lottie-react";
import salespeople from './salespeople.json';

function SalesPeopleList() {
  const [salesPeople, setSalesPeople] = useState([]);

  useEffect(() => {
    async function loadSalesPeople() {
        try {
          const response = await fetch('http://localhost:8090/api/salespeople/');
          if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
          const data = await response.json();
          setSalesPeople(data.salespeople);
        } catch (error) {
          console.error(error);
        }
      }

    loadSalesPeople();
  }, []);

  return (
    <div>
    <h1 id="sales-title">SalesPeople</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {salesPeople.map(salesPerson => (
          <tr key={salesPerson.id}>
            <td>{salesPerson.employee_id}</td>
            <td>{salesPerson.first_name}</td>
            <td>{salesPerson.last_name}</td>
          </tr>
        ))}
      </tbody>
    </table>
    <Lottie
          animationData={salespeople}
          className="lottie-center-bottom"
          style={{
            height: "30%",
            width: "33.33%",
            zIndex: -2,
          }}
          loop={true}
        />
      </div>
  );
}

export default SalesPeopleList;
