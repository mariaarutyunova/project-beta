import Lottie from "lottie-react";
import cars from './cars.json';

function VehicleModelList(props) {
  if (!props.models) {
      return null;
  }

  return (
    <div>
      <h1 id="models-title">Models</h1>
      <table className="table table-striped">
          <thead>
              <tr>
                  <th>Name</th>
                  <th>Manufacturer</th>
                  <th>Picture</th>
              </tr>
          </thead>
          <tbody>
              {props.models.map(model => (
                  <tr key={model.href}>
                      <td>{model.name}</td>
                      <td>{model.manufacturer.name}</td>
                      <td><img src={model.picture_url} alt={model.name} width="100" /></td>
                  </tr>
              ))}
          </tbody>
      </table>
      <Lottie
          animationData={cars}
          className="lottie-center-bottom"
          style={{
            height: "30%",
            width: "33.33%",
            zIndex: -2,
          }}
          loop={true}
        />
      </div>
  );
}

export default VehicleModelList;
