import Lottie from "lottie-react";
import cars from './cars.json';

function AutomobileList(props) {

    if (!props.automobiles) {
        return null;
    }
  return (
    <div>
    <h1 id="models-title">Automobiles</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Vin</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {props.automobiles.map(automobile => (
          <tr key={automobile.href}>
            <td>{automobile.vin}</td>
            <td>{automobile.color}</td>
            <td>{automobile.year}</td>
            <td>{automobile.model.name}</td>
            <td>{automobile.model.manufacturer.name}</td>
            <td>{automobile.sold ? 'Yes' : 'No'}</td>
          </tr>
        ))}
      </tbody>
    </table>
    <Lottie
          animationData={cars}
          className="lottie-center-bottom"
          style={{
            height: "30%",
            width: "33.33%",
            zIndex: -2,
          }}
          loop={true}
        />
      </div>
  );
}

export default AutomobileList;
