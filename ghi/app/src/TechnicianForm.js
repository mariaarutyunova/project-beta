import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Lottie from "lottie-react";
import mechanic from './mechanic.json';

const TechnicianForm = () => {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        navigate('/technicians');
      } else {
        console.log('Error creating technician:', response.status);
      }
    } catch (error) {
      console.log('Error creating technician:', error);
    }
  };

  return (
    <div>
      <h1 id="tech-title">Create a Technician</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="first_name">First Name:</label>
          <input
            type="text"
            id="first_name"
            name="first_name"
            value={formData.first_name}
            onChange={handleChange}
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label htmlFor="last_name">Last Name:</label>
          <input
            type="text"
            id="last_name"
            name="last_name"
            value={formData.last_name}
            onChange={handleChange}
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label htmlFor="employee_id">Employee ID:</label>
          <input
            type="text"
            id="employee_id"
            name="employee_id"
            value={formData.employee_id}
            onChange={handleChange}
            className="form-control"

          />
        </div>
        <button type="submit" className="btn btn-primary">Create</button>
      </form>
      <Lottie
          animationData={mechanic}
          className="lottie-center-bottom"
          style={{
            height: "40%",
            width: "33.33%",
            zIndex: -2,
          }}
          loop={true}
        />
    </div>
  );
};

export default TechnicianForm;
