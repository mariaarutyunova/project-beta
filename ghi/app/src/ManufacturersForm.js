import React from 'react';
import Lottie from "lottie-react";
import cars from './cars.json';

function ManufacturersForm(props) {
  return (
    <div>
      <h1 id="manufacturers-title">Create a Manufacturer</h1>
    <form onSubmit={props.onSubmit}>
      <div className="form-group">
        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="name"
          name="name"
          value={props.name}
          onChange={props.onChange}
          className="form-control"
        />
      </div>
      <button type="submit" className="btn btn-primary">
        Add Manufacturer
      </button>
    </form>
          <Lottie
          animationData={cars}
          className="lottie-center-bottom"
          style={{
            height: "30%",
            width: "33.33%",
            zIndex: -2,
          }}
          loop={true}
        />
    </div>
  );
}

export default ManufacturersForm;
