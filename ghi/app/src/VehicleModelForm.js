import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Lottie from "lottie-react";
import cars from './cars.json';

function VehicleModelForm() {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [selectedManufacturer, setSelectedManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {
        async function loadManufacturers() {
            const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
                setSelectedManufacturer(data.manufacturers[0]?.id || '');
            }
        }
        loadManufacturers();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            name,
            picture_url: pictureUrl,
            manufacturer_id: Number(selectedManufacturer),
        };

        try {
            const response = await fetch('http://localhost:8100/api/models/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            });

            if (response.ok) {
                setName('');
                setPictureUrl('');
                setSelectedManufacturer(manufacturers[0]?.id || '');

                navigate('/models');
            } else {
                console.error('Failed to create the model:', await response.text());
            }
        } catch (error) {
            console.error('There was an error submitting the form:', error);
        }
    };


    return (
        <div>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1 id="models-title">Create a Vehicle Model</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                onChange={e => setName(e.target.value)}
                                value={name}
                                placeholder="Name"
                                required
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={e => setPictureUrl(e.target.value)}
                                value={pictureUrl}
                                placeholder="Picture URL"
                                required
                                type="url"
                                name="pictureUrl"
                                id="pictureUrl"
                                className="form-control"
                            />
                            <label htmlFor="pictureUrl">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select
                                onChange={e => setSelectedManufacturer(e.target.value)}
                                value={selectedManufacturer}
                                required
                                name="manufacturer"
                                id="manufacturer"
                                className="form-select"
                            >
                                <option value="">Choose a manufacturer</option>
                                {manufacturers.map((manufacturer) => (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                ))}
                            </select>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <button className="btn btn-primary" type="submit">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <Lottie
            animationData={cars}
            className="lottie-center-bottom"
            style={{
            height: "30%",
            width: "33.33%",
            zIndex: -2,
            }}
            loop={true}
            />
        </div>
    );
}

export default VehicleModelForm;
